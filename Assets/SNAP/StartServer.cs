﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Net;
using System.Threading;
using System.Linq;
using System.Text;

public class StartServer : MonoBehaviour
{
    public WebServer ws;
    Tracker tracker;

    public void Start()
    {
        Debug.Log("ciao".Split(':'));

        ws = new WebServer(SendResponse, "http://localhost:8080/");
        ws.Run();
        Debug.Log("Started server on port 8080");

        tracker = GameObject.Find("Main Camera").GetComponent<Tracker>();
    }

    // Respond to HTTP request
    public string SendResponse(HttpListenerRequest request)
    {
        string path = request.Url.LocalPath;
        string query = "";

        if (path.Contains(":"))
        {
            path = request.Url.LocalPath.Split(':')[0];
            query = request.Url.LocalPath.Split(':')[1];
        }
        
        Debug.Log("Path: " + path);
        Debug.Log("Query: " + query);

        switch (path)
        {
            case "/test":
                return "Hello World";

            case "/getpos":                
                return tracker.Track(query);

            default:
                return string.Format("<HTML><BODY>Welcome to ASPHI Snap extension webserver for Kinect!<br>{0}</BODY></HTML>", DateTime.Now);
        }
    }
}
