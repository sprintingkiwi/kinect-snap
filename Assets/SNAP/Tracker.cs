using UnityEngine;
using System.Collections;

public class Tracker : MonoBehaviour 
{
//	public Vector3 TopLeft;
//	public Vector3 TopRight;
//	public Vector3 BottomRight;
//	public Vector3 BottomLeft;

	public GUITexture backgroundImage;
	public KinectWrapper.NuiSkeletonPositionIndex TrackedJoint = KinectWrapper.NuiSkeletonPositionIndex.HandRight;
	public GameObject OverlayObject;
    public GameObject[] trackers = new GameObject[19];
    public string[] positions = new string[19];
	public float smoothFactor = 5f;
    public bool lerp;
    public string lastPos = "0,0,0";
	
	public GUIText debugText;

	private float distanceToCamera = 10f;


    void Start()
    {
        for (int i = 0; i < 19; i++)
        {
            trackers[i] = GameObject.CreatePrimitive(PrimitiveType.Cube);
            trackers[i].transform.position = new Vector3(0, 0, 5f);
            trackers[i].transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
        }

        if (OverlayObject)
        {
            distanceToCamera = (OverlayObject.transform.position - Camera.main.transform.position).magnitude;
        }

        KinectManager manager = KinectManager.Instance;

        if (manager && manager.IsInitialized())
        {
            if (backgroundImage && (backgroundImage.texture == null))
            {
                backgroundImage.texture = manager.GetUsersClrTex();
            }
        }
    }  

    void Update()
    {
		KinectManager manager = KinectManager.Instance;
		
		if(manager && manager.IsInitialized())
		{
			//if(backgroundImage && (backgroundImage.texture == null))
			//{
			//	backgroundImage.texture = manager.GetUsersClrTex();
			//}

            for (int index = 0; index < 19; index++)
            {
                if (manager.IsUserDetected())
                {
                    uint userId = manager.GetPlayer1ID();

                    if (manager.IsJointTracked(userId, index))
                    {
                        Vector3 posJoint = manager.GetRawSkeletonJointPos(userId, index);

                        if (posJoint != Vector3.zero)
                        {
                            // 3d position to depth
                            Vector2 posDepth = manager.GetDepthMapPosForJointPos(posJoint);

                            // depth pos to color pos
                            Vector2 posColor = manager.GetColorMapPosForDepthPos(posDepth);

                            float scaleX = (float)posColor.x / KinectWrapper.Constants.ColorImageWidth;
                            float scaleY = 1.0f - (float)posColor.y / KinectWrapper.Constants.ColorImageHeight;

                            if (debugText)
                            {
                                debugText.GetComponent<GUIText>().text = "Tracked user ID: " + userId;
                            }

                            Vector3 vPosOverlay = Camera.main.ViewportToWorldPoint(new Vector3(scaleX, scaleY, distanceToCamera));
                            if (lerp)
                            {
                                vPosOverlay = Vector3.Lerp(trackers[index].transform.position, vPosOverlay, smoothFactor * Time.deltaTime);
                            }
                            trackers[index].transform.position = vPosOverlay;
                            positions[index] = vPosOverlay.ToString().Replace(" ", "").Replace("(", "").Replace(")", "");
                        }
                    }
                }
            }
		}
	}

    public string Track(string index)
    {
        string response = positions[int.Parse(index)];
        lastPos = response;
        return response;
    }
}
